### Pfam Database information  

#### Version 33.1
PfamA.seed MSA https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam33.1/Pfam-A.seed.gz  
PfamA.clans https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam33.1/Pfam-A.clans.tsv.gz  
userman https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam33.1/userman.txt  
`Total families:` 18259  
`Families with clans:` 7165  
`Total clans:`	635

#### Version 35
PfamA.seed MSA https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam35.0/Pfam-A.seed.gz  
PfamA.full MSA https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam35.0/Pfam-A.full.gz  
Pfam-A.hmm.dat.gz https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam35.0/Pfam-A.hmm.dat.gz
PfamA.clans https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam35.0/Pfam-A.clans.tsv.gz  
userman https://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam35.0/userman.txt  
`Total families:` 19633  
`Families with clans:` 7769  
`Total clans:`	655
# from libc.stdio cimport FILE, sprintf
# from libc.stdlib cimport atoi, atof, atol
# from libc.math cimport ceil

# cdef extern from "stdio.h":
#     FILE *fopen(const char *, const char *)
#     int fclose(FILE *)
#     ssize_t getline(char **, size_t *, FILE *)

# cdef str create_corpus_file(str rawDocFilePath, str outDocFilePath, long maxRep=*, bint addTags=*, bint saveAsPickle=*)


# SonicParanoid: fast, accurate, and easy orthology

> SonicParanoid is a stand-alone software tool for the identification of orthologous relationships among multiple species.  
> Aside from the trational graph-based approach, it uses Language Models to infer orthology at the domain level.

![Inline image](./docs/assets/sonic2-pipeline.png)

Documentation and examples on how to use it can be found in the [`wiki`](https://gitlab.com/salvo981/sonicparanoid2/-/wikis/home)

### <a href="https://gitlab.com/salvo981/sonicparanoid2/-/wikis/home"><img src="./docs/assets/book-half.svg" height="25"> Documentation <img src="./docs/assets/book-half.svg" height="25"></a>

>#### Citation  
> Salvatore Cosentino and Wataru Iwasaki (2023)  
> *SonicParanoid2: fast, accurate, and comprehensive orthology inference with machine learning and language models.*  
> **bioRxiv, 2023.05.14.540736**,  
> DOI: https://doi.org/10.1101/2023.05.14.540736
>
> Salvatore Cosentino and Wataru Iwasaki (2019)  
> *SonicParanoid: fast, accurate, and easy orthology inference.*  
> **Bioinformatics, Volume 35, Issue 1, 1 January 2019, Pages 149–151,**  
> DOI: https://doi.org/10.1093/bioinformatics/bty631 


## Fast and Scalabe

SonicParanoid is able to infer the orthologs for dozens of prokaryotes in minutes, or hours for eukaryotes, using a desktop computer with 8 CPUs. This figure is much smaller when running on HPC servers with dozens of CPUs (e.g. <1h for the QfO benchmark dataset). It is also highly scalable, as it inferred the orthologs for 2000 MAGs in only 1 day using 128 CPUs.


## Accurate

SonicParanoid was tested using a benchmark proteome dataset from the [the Quest for Orthologs consortium](http://questfororthologs.org), and the correctness of its predictions was evaluated using the [QfO Benchmarking service](https://orthology.benchmarkservice.org). SonicParanoid showed the highest accuracy in the aggregated rankings from the three accuracy classification methods in the 2020 QfO benchmark.


## Easy to use

SonicParanoid only requires the Python programming language and a GNU GCC compiler to be installed in your laptop/server in order to work. The low hardware requirements make it possible to run SonicParanoid on modern laptop computers, while the "update" feature allows users to easily maintain collections of orthologs that can be updated by adding or removing species.

## <img src="./docs/assets/journal-medical.svg" height="25"> License and Links <img src="./docs/assets/link-45deg.svg" height="25">

Copyright &copy; 2017, Salvatore Cosentino, [The University of Tokyo](http://www.u-tokyo.ac.jp/en/index.html) All rights reserved.

Licensed under the GNU GENERAL PUBLIC LICENSE, Version 3.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
[https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an __"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND__, either express or implied. See the License for the specific language governing permissions and limitations under the License.

## <img src="./docs/assets/person-vcard.svg" height="25"> Contact <img src="./docs/assets/person-vcard.svg" height="25">
`Salvatore Cosentino`

* salvocos@k.u-tokyo.ac.jp
* salvo981@gmail.com

`Wataru Iwasaki`

* email: iwasaki@k.u-tokyo.ac.jp

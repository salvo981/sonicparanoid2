#=
Convert MCL input file into different formats
=#

using Printf:@printf,@sprintf
using DocOpt
using Logging

# Command line interface
doc = """
Usage: mcl_input_converter.jl (-i <in_tbl>) (-o <out_tbl>) [-d --in-mtx --in-abc --in-hipmcl --abc --hipmcl]

Options:
    -i <in_tbl>, --tbl         MCL input file (it can in matrix, abc, or hipmcl formats)
    -o <out_dir>, --out-tbl    Path to the output [default: .]
    --in-mtx                   Input is in matrix format format. [default: true]
    --in-abc                   Input is in abc format.
    --in-hipmcl                Input is in hipmcl format.
    --abc                      Convert to abc format.
    --hipmcl                   Convert to hipmcl format.
    -d, --debug                Debug mode.
"""



"""
    Verfy that the input format is the one specified by the user
"""
function validate_input_fmt(inTbl::String, fmt::String="mtx")
    # This function will check only the first line of the input table
    fmtlist::Array{String, 1} = ["mtx", "abc", "hipmcl"]

    if fmt ∉ fmtlist
        @error "Not valid input format" fmt
        exit(-7)
    end

    @debug "validate_input_fmt :: START" inTbl fmt

    # extract the first line
    line::String = readline(inTbl)
    example::String = ""
    if fmt == "mtx"
        if line != "(mclheader"
            @error "The input is not formatted in matrix format" line
            @info "Valid matrix format starts must start with '(mclheader'"
            exit(-6)
        end
    elseif fmt == "abc"
        example = "vertex_1 vertex_2 0.34"
        if length(split(line, " ")) != 3
            @error "The input is not in abc format" line
            @info "Valid abc format must contain a space-separated line for each vertex" example
            exit(-6)
        end
    elseif fmt == "hipmcl"
        example = "vertex_1\tvertex_2\t0.34"
        if length(split(line, "\t")) != 3
            @error "The input is not in hipmcl format" line
            @info "Valid hipmcl format must contain a tab-separated line for each vertex" example
            exit(-6)
        end
    end

end



"""Convert an MCL input file from matrix format to either abc or hipmcl compatible format
"""
function convert_mtx2flat(inTbl::String, outTbl::String, outFmt::String=false)
    @debug "convert_mtx2flat :: START" inTbl outTbl outFmt

    hipMcl::Bool = false
    if outFmt == "hipmcl"
        hipMcl = true
    end

    if outfmt ∉ ["hipmcl", "abc"]
        @error "Invalid output format" outfmt
        exit(-7)
    end

    outDir::String = dirname(outTbl)
    if hipMcl
        outTbl = joinpath(outDir, "hipmcl.in.$(basename(inTbl))")
    else
        outTbl = joinpath(outDir, "abc.in.$(basename(inTbl))")
    end

    # open the output file
    ofd::IOStream = open(outTbl, "w")

    # tmp variables
    outln::String = mainNode = ""
    flds::Array{String, 1} = []
    
    # open the input mtx file and skip the first 8 header lines
    ifd::IOStream = open(inTbl, "r")
    readline(ifd, keep=true)
    readline(ifd, keep=true)
    @info "Input Matrix dimensions" readline(ifd)
    readline(ifd, keep=true)
    readline(ifd, keep=true)
    readline(ifd, keep=true)
    readline(ifd, keep=true)
    readline(ifd, keep=true)

    # Format each entry and write it in the wanted new format
    # Example input lines
    #0    $
    #1    3336:1 5606:1 8538:1 10081:0.638 13087:1 18208:1 $
    #2    3592:1 5362:1 $
    #3    4952:1 13034:1 $
    #4    $

    for ln in eachline(ifd, keep=false)
        # Stop at last line
        if ln[1] == ')' break end

        flds = split(ln, "    ")
        # skip empty entries
        if length(flds[2]) == 1
            continue
        else
            mainNode = flds[1]
            flds = split(flds[2], " ")
            for connection in flds[1:end-1]
                # replace the ':' with the appropriate character
                if hipMcl
                    connection = replace(connection, ":"=>"\t")
                    write(ofd, "$(mainNode)\t$(connection)\n")
                else
                    connection = replace(connection, ":"=>" ")
                    write(ofd, "$(mainNode) $(connection)\n")
                end
            end
        end
    end

    # close input and output files
    close(ifd)
    close(ofd)
end



"""Convert an MCL input file from abc format to hipmcl format
"""
function convert_abc2hipmcl(inTbl::String, outTbl::String)
    @debug "convert_abc2hipmcl :: START" inTbl outTbl

    outDir::String = dirname(outTbl)
    outTbl = joinpath(outDir, "hipmcl.in.$(basename(inTbl))")
    # open the output file
    ofd::IOStream = open(outTbl, "w")
    ifd::IOStream = open(inTbl, "r")
    # Simply replace spaces with TAb in the orignsl in puit
    for ln in eachline(ifd, keep=true)
        write(ofd, replace(ln, " "=>"\t"))
    end
    # close input and output files
    close(ifd)
    close(ofd)
end



"""Convert an MCL input file from hipmcl format to abc format
"""
function convert_hipmcl2abc(inTbl::String, outTbl::String)
    @debug "convert_abc2hipmcl :: START" inTbl outTbl

    outDir::String = dirname(outTbl)
    outTbl = joinpath(outDir, "abc.in.$(basename(inTbl))")
    # open the output file
    ofd::IOStream = open(outTbl, "w")
    ifd::IOStream = open(inTbl, "r")
    # Simply replace spaces with TAb in the orignsl in puit
    for ln in eachline(ifd, keep=true)
        write(ofd, replace(ln, "\t"=>" "))
    end
    # close input and output files
    close(ifd)
    close(ofd)
end



##### MAIN #####

# obtain CLI arguments
args = docopt(doc)
inTbl = realpath(args["--tbl"])
outTbl = abspath(args["--out-tbl"])
outDir = dirname(outTbl)
inabc = args["--in-abc"]
inhipmcl = args["--in-hipmcl"]
outabc = args["--abc"]
outhipmcl = args["--hipmcl"]

debug = args["--debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

# Create and activate the logger
mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger


# Set the input format
infmt = outfmt = ""
# check which input format was provided
if inabc
    infmt = "abc"
end
if inhipmcl
    infmt = "hipmcl"
end

if inhipmcl && inabc
    @error "--in-hipmcl and --in-abc cannot be used at the same time"
    exit(-7)
end

# set the output format
if outabc
    outfmt = "abc"
end
if outhipmcl
    outfmt = "hipmcl"
end

if outhipmcl && outabc
    @error "--hipmcl and --abc cannot be used at the same time"
    exit(-7)
end

# Exit if conversion is not possible
if infmt == outfmt
    @info "Input and output formats are same, nothing wil be done!" infmt outfmt
end

# if abc and hipmcl formats are not in input then
# set the input format to standard matrix format
if !(inhipmcl || inabc)
    infmt = "mtx"
end

@debug "ARGs" inTbl outTbl inabc inhipmcl infmt outfmt

# Make sure the input is in a valid format
validate_input_fmt(inTbl, infmt)

# Convert to another format
if infmt == "mtx"
    convert_mtx2flat(inTbl, outTbl, outfmt)
elseif infmt == "abc"
    convert_abc2hipmcl(inTbl, outTbl)
elseif infmt == "hipmcl"
    convert_hipmcl2abc(inTbl, outTbl)
end
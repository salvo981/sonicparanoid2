"""
 Create PyPI packages for OSX and LINUX (source).
"""

import os
import sys
from shutil import which, copy
from subprocess import run
import platform
import argparse
from typing import TextIO



########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    parser = argparse.ArgumentParser(description=f"Generate python package to be uploaded in PyPI", usage='%(prog)s -o <OUTPUT_DIRECTORY>', prog="generate_pypi_packages.py")

    # Mandatory arguments
    parser.add_argument("-o", "--out-dir", type=str, required=True, help="Output directory.", default=None)
    parser.add_argument("-t", "--make-test-copy", required=False, help="Generate a copy of the package to be used for testing the installation.\n", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def extract_version(setupFilePath:str) -> str:
    """
    Obtain package version from setup.py
    """
    ver: str = ""
    tmpStr: str = ""

    # The line containing the version have the followibng format:
    # version = "2.0.0a2", # Required

    with open(setupFilePath, "rt") as ifd:
        for ln in ifd:
            ln = ln[:-1]
            if ln.startswith("    version = "):
                # print(ln)
                tmpStr = ln.split("\"", 1)[1]
                # print(tmpStr)
                ver = tmpStr.rsplit("\"", 1)[0]
                # print(ver)

    return ver




##### MAIN #####
def main() -> None:

    isDarwin = True
    # check OS
    OS = platform.system()
    '''
    if not OS == 'Darwin':
        isDarwin = False
        sys.stderr.write("WARNING: you must use a MacOS system to generate the packages bdist binary package.")
        sys.stderr.write("\nThe MacOS bdist package will not be created.\n")
    '''

    #Get the parameters
    args, parser = get_params()
    del parser
    # Output directory
    outDir: str = os.path.abspath(args.out_dir)
    if not os.path.isdir(outDir):
        os.makedirs(outDir)

    # check python installation
    pythonPath: str = ""
    if which("python3") is None:
        sys.stderr.write("ERROR: the command \"python3\" was not found. Please install python3.")
        sys.exit(-5)
    else:
        pythonPath = which("python3")

    # set log paths
    # get path to the script
    pySrcDir: str = os.path.dirname(os.path.abspath(__file__))
    setupPath: str = os.path.join(os.path.dirname(pySrcDir), "setup.py")
    # check the extans of the setup file
    if not  os.path.isfile(setupPath):
        sys.stderr.write(f"ERROR: the setup file \n{setupPath}\n was not found in the directory\n{pySrcDir}\n")
        sys.exit(-2)

    # extract the version of SonicParanoid
    sonicVer: str = extract_version(setupPath)
    # sys.exit("DEBUG")

    # generate the sdist source packages
    stdoutPath: str = os.path.join(outDir, "generate_sdist.stdout.txt")
    stderrPath: str = os.path.join(outDir, "generate_sdist.stderr.txt")

    # create the log files
    fdout: TextIO = open(stdoutPath, "w")
    fderr: TextIO = open(stderrPath, "w")
    bldCmd: str = f"{pythonPath} {setupPath} sdist  --dist-dir {outDir}"

    # Print some infomration
    sys.stdout.write("\nPyPI package creation info:")
    sys.stdout.write(f"\nOS: {OS}")
    sys.stdout.write(f"\nPython path: {pythonPath}")
    sys.stdout.write(f"\nsetup.py path: {setupPath}")
    sys.stdout.write(f"\nSonicParanoid version: {sonicVer}")
    sys.stdout.write(f"\nOutput directory path: {outDir}")
    sys.stdout.write(f"\nOutput directory exists: {os.path.isdir(outDir)}")
    sys.stdout.write(f"\nBuild CMD: {bldCmd}")

    # use run
    # run(bldCmd, shell=True, stdout=fdout, stderr=fderr)
    run(bldCmd, shell=True)

    # close the log files
    fdout.close()
    fderr.close()

    # Identify the path with the sdist package
    pkgPath: str = ""
    sys.stderr.write(f"\nSONIC-DEBUG: {stdoutPath}")
    sys.stderr.write(f"\nSONIC-DEBUG: os.path.isfile(stdoutPath) == {os.path.isfile(stdoutPath)}\n")

    pkgPath = os.path.join(outDir, f"sonicparanoid-{sonicVer}.tar.gz")

    if os.path.isfile(pkgPath):
        print(f"\nPackage found at:\n{pkgPath}\n")
    else:
        sys.stderr.write(f"\nERROR: the package file for {os.path.basename(pkgPath)} was not found!\n")

    if args.make_test_copy:
        copy(pkgPath, os.path.join(outDir, "sonicparanoid.test.tar.gz"))
        print(f"\nINFO: a copy of the package for testing was created in the output directory\n{outDir}")

    # print some info on how to upload the packages
    print(f"\nThe package file {os.path.basename(pkgPath)} for PyPI was successfully generated.")
    print(f"To upload it to PyPI run the following command:")
    print(f"twine upload --sign {pkgPath}")
    print("\nor to upload a test version, execute the following:")
    print(f"twine upload --repository-url https://test.pypi.org/legacy/ --sign {pkgPath}")

    print("\nTo install sonicparanoid from the test PyPI repository use the following command:")
    print("pip3 install --index-url https://test.pypi.org/simple/ sonicparanoid")

if __name__ == "__main__":
    main()

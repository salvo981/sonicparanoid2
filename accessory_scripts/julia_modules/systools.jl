#=
Methods to interface the OS, for example check file types and create directories.
=#

using Printf: @printf, @sprintf
using Logging: @warn, @debug, @info


struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Extract a single file from TAR archive"""
function extract_file_from_tar(tarPath::String, fileToExtract::String, outDir::String)::Int
    @debug "extract_file_from_tar :: START" tarPath  fileToExtract outDir

    if !isfile(tarPath)
        @error "The TAR archive does not exist!" tarPath
        exit(-2)
    end

    if length(fileToExtract) == 0
        @error "The name of the file to extract cannot be empty" fileToExtract
        exit(-5)
    end

    # Create output directory
    makedir(outDir)

    # Change the directory before running the command
    startDir::String = pwd()
    cd(outDir)

    # Example of command to extract file
    # tar --extract --file=faa.tar faa/3300029549_26.faa.gz

    # Run the tar command to extract the file
    tarcmd::Cmd = `tar --extract --file=$(tarPath) $(fileToExtract)`

    # Open the image
    procOut::Base.Process = run(pipeline(tarcmd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        @error "Something went extracting the file from TAR:" tarPath fileToExtract
        exit(-5)
    end

    # Reset the current working directory
    cd(startDir)

    return procOut.exitcode
end



"""Extract all files from TAR archive"""
function extract_tar_archive(tarPath::String, outDir::String)::Int
    @debug "extract_tar_archive :: START" tarPath outDir

    if !isfile(tarPath)
        @error "The TAR archive does not exist!" tarPath
        exit(-2)
    end
    # Create output directory
    makedir(outDir)

    # Change the directory before running the command
    startDir::String = pwd()
    cd(outDir)

    # Example of command to extract file
    # tar --extract --file=faa.tar

    # Run the tar command to extract the file
    tarcmd::Cmd = `tar --extract --file=$(tarPath)`

    # Open the image
    procOut::Base.Process = run(pipeline(tarcmd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        @error "Something went extracting the TAR archive:" tarPath outDir
        exit(-5)
    end

    # Reset the current working directory
    cd(startDir)

    return procOut.exitcode
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("System tools", "systools.jl", "Salvatore Cosentino", "GPLv3", 0.1, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @show mod.name
        @show mod.source
        @show mod.author
        @show mod.license
        @show mod.version
        @show mod.maintainer
        @show mod.email
    end
    # return the module info struct
    return mod
end



"""Check if a file is compressed"""
function is_archive(fpath::String)::Tuple{Bool, String}
    @debug "is_archive::START" fpath

    # check the the input exists
    if !isfile(fpath)
        println("ERROR: the file $(fpath) does not exist!")
        exit(-2)
    end

    # valid formats
    fmts::Array{String, 1} = ["x-gzip", "x-zip", "gzip"]
    # prepare and execute the command to check the file type
    tmpCmd::Cmd = `file -b --mime-type $(fpath)`
    out::String = rstrip(read(tmpCmd, String), '\n')
    if split(out, "/", limit=2)[2] in fmts
        @debug "Archive format" out
        return (true, out)
    else
        return (false, out)
    end
end



"""Create a directory"""
function makedir(dpath::String)
    @debug "makedir :: START" dpath
    try
        mkdir(dpath, mode=0o751)
    catch e
        if !isdir(dirname(dpath))
            println("\nINFO: intermediate directories are missing and will be created.")
            mkpath(dpath, mode=0o751)
        else
            @debug "\nThe directory $(dpath) already exists."
        end
    end
end



"""Create archive using pigz"""
function pigz_compress(inpath::String, outpath::String; level::Int=9, keep::Bool=true, force::Bool=true, useGzip::Bool=true, errLog=false, threads::Int=4)::String
    @debug "pigz_compress :: START" inpath outpath level keep force useGzip errLog threads
    # prepare the system call create the archive
    options::Array{String, 1} = Array{String, 1}()
    outdir::String = dirname(outpath)
    stderrPath::String = joinpath(outdir, "stderr.pigz_compress.$(basename(outpath)).txt")

    if inpath == outpath
        @warn "Input and output paths are same. Use with caution." inpath outpath
    end
    # add special options if required
    if keep
        push!(options, "--keep")
    end

    if !useGzip
        push!(options, "--zip")
    end

    if force
        push!(options, "--force")
    end

    if errLog
        push!(options, "--verbose")
    end

    if useGzip
        outpath = "$(outpath).gz"
    else
        outpath = "$(outpath).zip"
    end

    # Prepare the command
    tmpCmd::Cmd = `pigz $(options) -c -p $(threads) -$(level) $(inpath)`
    if errLog
        run(pipeline(tmpCmd, stdout=outpath, stderr=stderrPath), wait=true)
    else
        run(pipeline(tmpCmd, stdout=outpath), wait=true)
    end

    # Remove original input if required
    if !keep
        if inpath != outpath
            rm(inpath, force=true)
        end
    end

    return outpath
end



"""Decompress archive using pigz"""
function pigz_decompress(inpath::String, outpath::String; keep::Bool=true, force::Bool=true, errLog=false, threads::Int=4)::Nothing
    @debug "pigz_decompress :: START" inpath outpath keep force threads

    if inpath == outpath
        @error "Input and output must be different." inpath outpath
        exit(-5)
    end
    # prepare the system call create the archive
    options::Array{String, 1} = Array{String, 1}()
    outdir::String = dirname(outpath)
    stderrPath::String = joinpath(outdir, "stderr.pigz_decompress.$(basename(outpath))")
    # add special options if required
    if keep
        push!(options, "--keep")
    end

    if force
        push!(options, "--force")
    end

    if errLog
        push!(options, "--verbose")
    end

    # Prepare the command
    tmpCmd::Cmd = `pigz $(options) -c -d -p $(threads) $(inpath)`
    if errLog
        run(pipeline(tmpCmd, stdout=outpath, stderr=stderrPath), wait=true)
    else
        run(pipeline(tmpCmd, stdout=outpath), wait=true)
    end

    # Remove original input if required
    if !keep
        if inpath != outpath
            rm(inpath, force=true)
        end
    end

    return nothing
end



"""Visualize image file"""
function open_img(imgPath::String)::Int
    # obtain system information
    islinux::Bool = Sys.islinux()
    @debug "open_img :: START" imgPath

    # Run the display command in linux
    viewcmd::Cmd = `display $(imgPath)`
    if !islinux
        if Sys.isapple()
            viewcmd = `open -a Preview $(imgPath)`
        else
            write(stderr, "\nWARNING: this software can open the image only on Linux or Apple OS based systems\n")
        end
    end

    # Open the image
    procOut::Base.Process = run(pipeline(viewcmd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went when trying to open the image file\n$(imgPath)")
        exit(-5)
    end

    return procOut.exitcode
end

#=
Utility methods for simple sequence analysis.
=#

using Logging: @warn, @debug, @info
using Distributed
using DataStructures
using Printf: @printf, @sprintf
using FASTX
using StatsBase: mean
using CodecZlib: GzipDecompressorStream
include("./systools.jl")



struct ModuleInfo
    name::String
    source::String
    author::String
    license::String
    version::Float16
    maintainer::String
    email::String
end



"""Print module info."""
function info(show::Bool=false)::ModuleInfo
    mod = ModuleInfo("Sequence tools", "seq_tools.jl", "Salvatore Cosentino", "GPLv3", 0.3, "Salvatore Cosentino", "salvo981@gmail.com")
    if show
        @info mod.name mod.source mod.author mod.license mod.version mod.maintainer mod.email
    end
    # return the module info struct
    return mod
end



"""
Contains stats for a FASTA sequence.

fields: cnt, total, max, min, avg.
"""
struct FastaStats
    cnt::UInt32
    maxlen::UInt32
    minlen::UInt32
    totlen::UInt32
    avglen::Float64
end

# Summary and show functions for FastaStats
function Base.summary(io::IO, s::FastaStats)
    print(io, "FastaStats:\nCount\t$(s.cnt)\nLongest\t$(s.maxlen)\nShortest\t$(s.minlen)\nTotal\t$(s.totlen)\nAverage\t$(s.avglen)\n")
end

Base.show(io::IO, s::FastaStats) = summary(io, s)



"""Check the sequence format.
   Returns a string with the sequence format and a boolean saying if the file is compressed
"""
function check_seq_fmt(fpath::String)::Tuple{String, Bool}
    @debug "check_seq_fmt :: START" fpath
    # check if the file is compressed
    archiveFmt::String = seqFmt = ln = ""
    compressed::Bool = true
    c::Char = ' '
    compressed, archiveFmt = is_archive(fpath)
    # read the compressed stream if compressed
    if compressed
        c = readline(GzipDecompressorStream(open(fpath, "r") ;windowbits=15, gziponly=false))[1]
    else
        c = readline(fpath, keep=true)[1]
    end
    # Set the sequence format after checking the first char
    if c == '@'
        seqFmt = "fastq"
    elseif c == '>'
        seqFmt = "fasta"
    else
        @error "Unknown sequence format." c
        exit(-5)
    end

    return (seqFmt, compressed)
end



"""Count sequences in FASTA file"""
function count_seqs(fpath::String)::Tuple{String,Int}
    @debug "count_seqs :: START" fpath
    if !isfile(fpath)
        write(Base.stderr, "\nInput FASTA not found!")
        exit(2)
    else
        open(fpath, "r") do ifd
            return (fpath, length([x=1 for x in eachline(ifd) if x[1] == '>']))
        end
    end
end



"""Compute sequences statistics"""
function compute_fasta_stats(fpath::String)::FastaStats
    @debug "compute_fasta_stats :: START" fpath

    # NOTE: it is not clear why casting the length to Int creates a little less allocations than casting to UInt32. Also, not casting at all is vene a little slower

    # Extract the sequence count
    seqLengths::Array{UInt32, 1} = [Int(length(sequence(biorec))) for biorec in open(FASTA.Reader, fpath)]

    # Compute stats
    outSt::FastaStats = FastaStats(length(seqLengths), maximum(seqLengths), minimum(seqLengths), sum(seqLengths), mean(seqLengths))

    @debug "" show(outSt)
    return outSt
end



"""Filter FASTA sequences by length"""
function filter_by_len(fpath::String, outpath::String, minlen::Int64=250)::Int64
    @debug "\nfilter_by_len :: START" fpath outpath minlen

    if !isfile(fpath)
        write(Base.stderr, "\nInput FASTA not found!")
        exit(2)
    end

    # check problems with outpath
    if ispath(fpath)
        if outpath == fpath
            write(Base.stderr, "ERROR: the input output file paths are identical!")
            exit(-2)
        end
    end

    # temp variables
    outdir::String = dirname(outpath)
    scnt::Int64 = rmcnt = 0
    tmpSeq = BioSequence
    # create the output directory if required
    makedir(outdir)
    
    # Process the input sequences
    freader = open(FASTA.Reader, fpath)
    # open output file
    ofd::IOStream = open(outpath, "w")
    for seq in freader
        scnt += 1
        tmpSeq = sequence(seq)
        
        if length(tmpSeq) >= minlen
            # write the sequence to the output file
            write(ofd, @sprintf(">%s\n%s\n", seqname(seq), convert(String, tmpSeq)))
        else
            rmcnt += 1
            continue
        end
    end
    close(freader)
    close(ofd)

    @debug """
    Total: $scnt
    Removed: $rmcnt
    outDir: $outDir
    Longer than $(minlen)Bps:\t$(scnt-rmcnt)
    """

    # Return number of remaining sequences
    return scnt-rmcnt
end



"""Count RAW reads in FASTSQ file"""
function count_reads(fpath::String)::Tuple{String,Int}
    @debug "count_reads :: START" fpath

    if !isfile(fpath)
        write(Base.stderr, "\nInput FASTQ not found!")
        exit(2)
    else
        cnt::Int = rcnt = 0
        for ln in eachline(fpath, keep=true)
            cnt += 1
            if ln[1] == '@'
                rcnt += 1
            end
        end
        # check that the number of counted lines is 4x the number of reads
        if (cnt/4) != rcnt
            println("ERROR: the number of reads ($(rcnt)) must 4x ($(cnt/rcnt)) the number of found read headers ($(rcnt))")
            exit(-5)
        else
            return (fpath, rcnt)
        end
    end
end



"""Add a prefix, suffix, or replace a FASTA header"""
function modify_fasta_hdr(fpath::String, outpath::String, mapFile::String; prefix::String="", suffix::String="", extendLeft::Bool=false)::Nothing
    @debug "modify_fasta_hdr :: START" fpath outpath mapFile prefix suffix extendLeft

    # tmp variables
    oldHdr::String = newHdr = ""
    mapDict::Dict{String, String} = Dict{String, String}()
    flds::Array{String, 1} = Array{String, 1}()

    # Load the mapping IDs in dictionary
    for ln in eachline(mapFile)
        flds = split(ln, "\t", limit=3)
        if !haskey(mapDict, flds[1])
            mapDict[flds[1]] = flds[2]
        end
    end

    # create the output directory if required
    makedir(dirname(outpath))
    # Process the input sequences
    freader = FASTA.Reader(open(fpath, "r"))
    # open output file
    ofd = FASTA.Writer(open(outpath, "w"))

    for seq in freader
        tmpSeq = sequence(seq)
        oldHdr = identifier(seq)
        newHdr = mapDict[oldHdr]
        # Extend the old HDR with the new one as prefix
        if extendLeft
            newHdr = "$(newHdr).$(oldHdr)"
        end
        # add the prefix if present
        if length(prefix) > 0
            newHdr = "$(prefix).$(newHdr)"
        end
        write(ofd, FASTA.Record(newHdr, tmpSeq))
    end
    close(freader)
    close(ofd)
    return nothing
end



"""Merge FASTQ or FASTA sequences"""
function merge_reads(paths::Array{String, 1}, outPath::String, outfmt::String="fastq"; maxreads::Int=0, threads::Int=4, compress::Bool=false)::Int
    @debug "merge_reads"  length(paths) basename(outPath) outfmt maxreads threads compress

    isArchive::Bool = true
    tmpseqfmt::String = "fasta"
    outDir::String = dirname(outPath)
    tmpInPath::String = finalOutPath = tmpInArchive = ""
    wcnt::Int = totw = 0

    # open the master output file
    if outfmt == "fasta"
        ofd = open(FASTA.Writer, outPath)
    elseif outfmt == "fastq"
        ofd = open(FASTQ.Writer, outPath)
    end

    # For each path perform similar checks
    for p in paths
        # Make sure that input and output directories are different for safety
        if dirname(p) == outDir
            @error "Input and output directories must differ!" outDir dirname(p)
            exit(-5)
        end
        
        # Copy the original input inot the output directory
        tmpInPath = joinpath(outDir, basename(p))
        # Copy and force overwrite
        cp(p, tmpInPath, force=true)
        # check the sequence and archive formats
        tmpseqfmt, isArchive = check_seq_fmt(tmpInPath)

        # Stop if the the input and output formats are different
        if tmpseqfmt != outfmt
            @error "Input and output sequence formats must not differ" outfmt tmpseqfmt 
        end
        # decompress if required
        if isArchive
            tmpInArchive = tmpInPath
            tmpInPath = rsplit(tmpInArchive, ".", limit=2)[1]
            # keep must always be true
            pigz_decompress(tmpInArchive, tmpInPath, keep=true, force=true, errLog=true, threads=threads)

            # Remove the copy of the original file
            rm(tmpInArchive, force=false)
        end
        
        if outfmt == "fasta"
            sfd = open(FASTA.Reader, tmpInPath)
        elseif outfmt == "fastq"
            sfd = open(FASTQ.Reader, tmpInPath)
        end
        
        for seq in sfd
            if (maxreads == 0) || (wcnt < maxreads)
                write(ofd, seq)
                wcnt += 1
            else
                break
            end
        end

        close(sfd)
        # update the total write count
        totw += wcnt
        wcnt = 0
        @debug "Total sequences in output" totw
        # remove the uncompressed original input
        rm(tmpInPath, force=true)
    end
    close(ofd)

    # compress the final input if required
    if compress
        finalOutPath = pigz_compress(outPath, outPath, level=9, keep=false, force=true, useGzip=true, errLog=true, threads=threads)

        # remove the uncompressed concatenated file
        rm(outPath, force=true)
    else
        finalOutPath = outPath
    end

    # Return the reads count in the output sequence file
    return totw

end



"""Modify FASTA header for MAGs obained from the GEM database from JGI."""
function simplify_gem_mag_hdr(fpath::String, outpath::String)::Nothing
    @debug "simplify_uniprot_hdr :: START" fpath outpath
    # Reference paper: https://doi.org/10.1038/s41587-020-0718-6
    # The original MAGs and metadata were obtained from https://portal.nersc.gov/GEM/
    # Metadata file: /genomes/genome_metadata.tsv
    # MAGs containing protein sequences: /genomes/faa.tar

    # Original headers have the following format:
    # 3300013755.a:Ga0117777_1004146_1 # 2 # 364 # 1 # ID=1_1;partial=10;start_type=Edge;rbs_motif=None;rbs_spacer=None;gc_cont=0.438
    # The simplified HDr should only contain the '3300013755.a:Ga0117777_1004146_1' part
    # So the hdr would become '>3300013755.a:Ga0117777_1004146_1'

    newHdr::String = ""
    # create the output directory if required
    makedir(dirname(outpath))
    # Process the input sequences
    freader = FASTA.Reader(open(fpath, "r"))
    # open output file
    fwriter = FASTA.Writer(open(outpath, "w"), width=60)
    for seq in freader
        newHdr = split(identifier(seq), " ", limit=2)[1]
        write(fwriter, FASTA.Record(newHdr, sequence(seq)))
    end
    # Close reader and writer
    close(freader)
    close(fwriter)
    return nothing
end



"""Modify Uniprot FASTA header to contain only the protein/gene ID."""
function simplify_uniprot_hdr(fpath::String, outpath::String)::Nothing
    @debug "simplify_uniprot_hdr :: START" fpath outpath

    # Original Uniprot sequence headers have the following format:
    # >sp|P61240|RS27A_NANEQ 30S ribosomal protein S27ae OS=Nanoarchaeum equitans (strain Kin4-M) OX=228908 GN=rps27ae PE=3 SV=1
    # When processing the FASTA sequences Biosequences chops the HDR to the first space
    # So the hdr would become '>sp|P61240|RS27A_NANEQ'
    # in order to use these sequences in the QfO benchmarks only the second field should be kept
    # which in the above example is 'P61240'

    newHdr::String = ""
    # create the output directory if required
    makedir(dirname(outpath))
    # Process the input sequences
    freader = FASTA.Reader(open(fpath, "r"))
    # open output file
    fwriter = FASTA.Writer(open(outpath, "w"), width=60)
    for seq in freader
        newHdr = split(identifier(seq), "|", limit=3)[2]
        write(fwriter, FASTA.Record(newHdr, sequence(seq)))
    end
    # Close reader and writer
    close(freader)
    close(fwriter)
    return nothing
end



"""Read a sequence file and write the sequence lengths in a output file"""
function write_seq_lengths(fpath::String, rawtxt::Bool=true)::String
    @debug "write_seq_lengths :: START" fpath rawtxt

    if !isfile(fpath)
        write(Base.stderr, "\nInput FASTA not found!")
        exit(2)
    end

    # Outpath
    outpath = joinpath(dirname(fpath), "lengths.$(basename(fpath))")
    # temp variables
    scnt::Int = 0

    # Process the input sequences
    freader = open(FASTA.Reader, fpath)
    # open output file
    ofd::IOStream = open(outpath, "w")
    for seq in freader
        scnt += 1
        write(ofd, @sprintf("%s\t%d\n", seqname(seq), length(sequence(seq))))
    end
    close(freader)
    close(ofd)

    @debug """
    Total: $scnt
    """

    # Return number of remaining sequences
    return outpath
end



"""Count sequences in parallel."""
function parallel_count_reads(fPaths::Array{String, 1}, threads::Int=4)::OrderedDict{String, Int}
    @debug "\nparallel_count_reads :: START" length(fPaths) workers() threads
    # create a pool of Workers
    wp = WorkerPool(workers())
    # execute the computation
    c = pmap(count_reads, wp, fPaths, distributed=true)

    # fill the output dictionary keeping sorted same as the input array of paths
    # will contain only the basename and the number of sequences
    sortedCounts = OrderedDict{String, Int}()
    for (i, tpl) in enumerate(c)
        sortedCounts[basename(tpl[1])] = tpl[2]
    end

    if debug
        @show length(sortedCounts)
        @show sortedCounts
        println(fPaths)
    end
    # return the dictionary with reads counts
    sortedCounts
end



"""Count sequences in parallel."""
function parallel_count_proteins(fPaths::Array{String, 1}, outJdl::String, threads::Int=4)::OrderedDict{String, Int}
    @debug """parallel_count_proteins :: START
    Sys.CPU_THREADS:\t$(Sys.CPU_THREADS)
    Paths:\t$(length(fPaths))
    outJdl: $(outJdl)
    Threads:\t$(threads)
    workers():\t$(workers())
    """

    # will contain the Future for each path
    fs = Dict{Int,Future}()
    @sync for (i, fastaPath) in enumerate(fPaths)
        @async fs[i] = @spawn count_proteins(fastaPath)
    end

    res = Dict{Int,Tuple{String, Int}}()
    @sync for fut in fs
        @async res[fut[1]] = fetch(fut[2])
    end

    # fill the output dictionary keep the input sort of the input array of paths
    # will contain only the basename and the number of sequences
    sortedCounts = OrderedDict{String, Int}()
    for (i, fastaPath) in enumerate(fPaths)
        tpl = res[i]
        sortedCounts[basename(tpl[1])] = tpl[2]
    end

    if debug
        @show length(sortedCounts)
        @show sortedCounts
    end
    # Store the dictionary in a JLD2 file
    FileIO.save(outJdl, sortedCounts)

    sortedCounts
end



"""Count sequences in parallel."""
function parallel_count_proteins_pmap(fPaths::Array{String, 1}, outJdl::String, threads::Int=4)::OrderedDict{String, Int}
    @debug """parallel_count_proteins_pmap :: START
    Sys.CPU_THREADS:\t$(Sys.CPU_THREADS)
    Paths:\t$(length(fPaths))
    outJdl: $(outJdl)
    Threads:\t$(threads)
    workers():\t$(workers())
    """

    # create a pool of Workers
    wp = WorkerPool(workers())
    # execute the computation
    c = pmap(count_proteins, wp, fPaths, distributed=true)

    @show typeof(c)
    @show c

    # fill the output dictionary keep the input sort of the input array of paths
    # will contain only the basename and the number of sequences
    sortedCounts = OrderedDict{String, Int}()
    for (i, tpl) in enumerate(c)
        sortedCounts[basename(tpl[1])] = tpl[2]
    end

    if debug
        @show length(sortedCounts)
        @show sortedCounts
    end
    # Store the dictionary in a JLD2 file
    FileIO.save(outJdl, sortedCounts)

    sortedCounts
end
'''
A table with exectution times, and a snapshot file from a SonicParanoid run, generate training for Adaboost model.
This algorithm makes sure that there is no negative value in the training set.
Negative values are not allowed in many ML algorithm in sklearn since version 1.3.0.
'''

import os
import sys
from typing import TextIO, List, Set, Dict, Tuple
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from itertools import combinations
import numpy as np
from sklearn import metrics


########### FUNCTIONS ############
def get_params():
    """Obtain input parameters."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="Generate training data for Adaboost.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-r", "--raw-ex-time", type=str, required=True, help="TSV file with execution times from SonicParanoid\n", default=None)
    parser.add_argument("-s", "--snapshot", type=str, required=True, help="TSV snapshot file from SonicParanoid run.\n", default=None)
    parser.add_argument("-p", "--prediction-tbl", type=str, required=False, help="TSV file with the predicted Fastest pairs from SonicParanoid.\n", default="")
    parser.add_argument("-f", "--fasta-dir", type=str, required=True, help="Directory with the input FASTA files.")
    parser.add_argument("-o", "--output-dir", type=str, required=True, help="Output directory.", default=os.getcwd())
    parser.add_argument("--allow-negative-values", required=False, help="By default negative values are not allowed in the training samples.\n", action="store_true")
    parser.add_argument("--mantissa", type=int, required=False, help="A positive value that is added by default when computing the folds. This value is used to avoid that negative values are added to the training data.\nFor example if the fold is -45x than the value in the sample will be 500-45=445; if folds is 45x then it is 500+45=545.", default=250)
    parser.add_argument("--accuracy-only", required=False, help="Compute only accuracy, assuming the table with training data is already available.\n", default=False, action="store_true")
    parser.add_argument("--categorical", required=False, help="USe categories (1 | 0) for the features representing the differences. The matissa will be ignored.\n", default=False, action="store_true")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



""" Other functions """
def check_magnitude_difference(v1:float, v2:float, xDiffThr:float, debug:bool=False) -> Tuple[int, float]:
    """
    Check the difference in magnitude between two values.
    This can be used for example to compare length difference,
    or the arch sizes

    Return a tuple of type (1, 1.5) where 1 indicates that the xDiffThr was surpassed.
    The last value says how many times one value is smaller (or bigger) than the other (e.g., 2X)
    If the first value is 0 then the threshold was not surpassed
    """
    if debug:
        print(f"\ncheck_magnitude_difference :: START")
        print(f"v1:\t{v1}")
        print(f"v2:\t{v2}")
        print(f"Length difference threshold:\t{xDiffThr}")
    # simply return that it should be kept
    if v1 == v2:
        return(0, 1.)
    else:
        # how many times v2 is bigger than v1
        v1VSv2:float = 1.
        # this should be a negative multiplier if v1 is bigger than v2
        # consider which value is the biggest:
        if v2 < v1:
            v1VSv2 = -round(float(v1)/float(v2), 2)
            # print(v1VSv2)
            # print(-(float(v1)/float(v2)))
            # print(abs(-(float(v1)/float(v2))))

        else: # v2 is bigger
            v1VSv2 = round(float(v2)/float(v1), 2)
            # v1VSv2 = float(v2)/float(v1)
            # print(type(v1VSv2))

        # Check if the threhold has been surpassed
        # and return the output tuple accordingly
        if abs(v1VSv2) > xDiffThr: # drop it
            # if v1VSv2 < 0:
                # print(v1VSv2, abs(v1VSv2))
                # sys.exit("DEBUG")
            return(1, v1VSv2)
        else: # keep it
            # print(v1VSv2, abs(v1VSv2))
            return(0, v1VSv2)



def compute_accuracy(trainingSmpls: str, predTblPath : str, snapshotPath : str, exTimeDict: Dict[str, int]) -> Tuple[str, Dict[str, int]]:
    """
    Compute different accuracy parameters using species, execution times and predictions.
    """
    logging.info(f"compute_accuracy :: START")
    logging.info(f"Training data table: {trainingSmpls}")
    logging.info(f"Table with predictions from SonicParanoid: {predTblPath}")
    logging.info(f"Table with species information from SonicParanoid run: {snapshotPath}")
    logging.info(f"Dictionary with execution times for each pair:\t{len(exTimeDict)}")

    # Set temp variables
    mappingDict: Dict[str, str] = {}
    predDict: Dict[str, int] = {}
    flds: List[str] = []
    spName: str = ""
    combinationClass: str = ""
    pairMain: str = ""
    prediction: int = 0
    fastestPair: int = 0
    extimeDiff: float = 0.
    cntDiff: float = 0.
    sizeDiff: float = 0.
    lenDiff: float = 0.
    weightDiff: float = 0.
    id1: str =""
    id2: str = ""
    sp1: str =""
    sp2: str = ""
    bname: str = os.path.basename(trainingSmpls)
    errTbl = bname.split(".", 1)[-1]
    outDir: str = os.path.dirname(trainingSmpls)
    predLabel: str = "" # tp, tn, fp, fn
    # The values of this tuple reprent the error type tp, tn, fp, fn
    predOutcomeTpl: Tuple[int, int, int, int] = (0, 0, 0, 0)
    errTbl = os.path.join(outDir, f"prediction_outcome.{errTbl}")
    confusionMtx: Dict[str, int] = {"tp":0, "tn":0, "fp":0, "fn":0}
    # actual fastest or slowest
    correctList: List[int] = []
    # predicted as fastest or slowest
    predList: List[int] = []

    # Load species information and fill mapping dictionary
    # Lines in the snapshot have the following format
    # sp_id species file_hash protein_cnt proteome_size
    # 6 clupus 84a09b94943956211e53faf2df1c0b76e2459910541ff237b8faaffdd15720c9 20649 11583642
    with open(snapshotPath, "rt") as ifd:
        for ln in ifd:
            ln = ln.rstrip("\n")
            flds = ln.split("\t", 4)
            spId = flds[0]
            spName = flds[1]
            # Add elements to mapping file
            mappingDict[spId] = spName

    # print(len(mappingDict))

    # Load predictions
    # Lines have the following format:
    # pair seq_cnt_a seq_cnt_b seq_cnt_diff_folds_b_gt_a proteome_size_a proteome_size_b prot_size_diff_folds_b_gt_a avg_seq_len_a avg_seq_len_b pred
    # 1-2 11501 11204 -1.027 6025983 4609745 -1.307 523.953 411.437 0
    ifd: TextIO = open(predTblPath, "rt")
    ifd.readline()
    for ln in ifd:
        ln = ln.rstrip("\n")
        pairIds = ln.split("\t", 1)[0]
        id1, id2 = pairIds.split("-", 1)
        sp1 = mappingDict[id1]
        sp2 = mappingDict[id2]
        pairMain = f"{sp1}-{sp2}"
        # add the prediction in the dictionary
        predDict[pairMain] = int(ln.rsplit("\t", 1)[-1])
    ifd.close()

    # create file with error types
    ofd: TextIO = open(errTbl, "wt")
    # print(errTbl)
    # hdr: str = "pair\tseq_cnt_a\tseq_cnt_b\tseq_cnt_diff_folds_b_gt_a\tproteome_size_a\tproteome_size_b\tprot_size_diff_folds_b_gt_a\tavg_seq_len_a\tavg_seq_len_b\tpred"

    hdr: str = "pair\tex_time_diff_folds\tseq_cnt_diff_folds\tprot_size_diff_folds\tavg_seq_len_diff_folds\tmol_mass_diff_folds\tfolds_combination_class\tfastest_pair\tprediction\ttp\ttn\tfp\tfn\n"
    ofd.write(hdr)

    wtCnt: int = 0

    # Lines in training data table have the following format
    # pair extime extime_diff_folds seq_cnt_a seq_cnt_b seq_cnt_diff_folds_b_gt_a proteome_size_a proteome_size_b prot_size_diff_folds_b_gt_a avg_seq_len_a avg_seq_len_b avg_seq_len_diff_folds_b_gt_a mol_mass_a mol_mass_b mol_mass_diff_folds_b_gt_a folds_combination_class	fastest_pair
    # aanophagefferens-abisporus 175.90 1.07 11501 11204 -1.03 6025983 4609745 -1.31 523.95 411.44 -1.27 65720.54 53142.25 -1.24 --- 1
    ifd = open(trainingSmpls, "rt")
    ifd.readline()
    for ln in ifd:
        flds = ln[:-1].split("\t", 16)
        sp1, sp2 = flds[0].split("-", 1)
        # extract the prediction
        pairMain = f"{sp1}-{sp2}"

        '''
        if pairMain in predDict:
            prediction = predDict[pairMain]
        else:
            prediction = predDict[f"{sp2}-{sp1}"]
        '''

        if not pairMain in predDict:
            continue
        else:
            prediction = predDict[pairMain]

        fastestPair = int(flds[-1])
        extimeDiff = float(flds[2])
        cntDiff = float(flds[5])
        sizeDiff = float(flds[8])
        lenDiff = float(flds[11])
        weightDiff = float(flds[14])
        combinationClass = flds[15]

        # Set the prediction outcome
        if prediction == 1: # Then it can be TP or FP
            predList.append(1)
            if fastestPair:
                correctList.append(1)
                # tp, tn, fp, fn
                predLabel = "tp"
                predOutcomeTpl = (1, 0, 0, 0)
                confusionMtx[predLabel] += 1
            else:
                correctList.append(0)
                predLabel = "fp"
                predOutcomeTpl = (0, 0, 1, 0)
                confusionMtx[predLabel] += 1
        else: # Then it can be TN or FN
            predList.append(0)
            if fastestPair:
                correctList.append(1)
                predLabel = "fn"
                predOutcomeTpl = (0, 0, 0, 1)
                confusionMtx[predLabel] += 1
            else:
                correctList.append(0)
                predLabel = "tn"
                predOutcomeTpl = (0, 1, 0, 0)
                confusionMtx[predLabel] += 1

        # "\t".join([str(x) for x in tpl2])
        wtCnt += 1

        outcomeStr: str = "\t".join([str(x) for x in predOutcomeTpl])
        # pair ex_time_diff_folds seq_cnt_diff_folds prot_size_diff_folds avg_seq_len_diff_folds mol_mass_diff_folds folds_combination_class fastest_pair prediction tp tn fp fn
        ofd.write(f"{pairMain}\t{extimeDiff}\t{cntDiff}\t{sizeDiff}\t{lenDiff}\t{weightDiff}\t{combinationClass}\t{fastestPair}\t{prediction}\t{outcomeStr}\n")

        # if wtCnt == 100:
        #     break

    ofd.close()

    # Compute accuracy
    tp: int = confusionMtx["tp"]
    tn: int = confusionMtx["tn"]
    fp: int = confusionMtx["fp"]
    fn: int = confusionMtx["fn"]

    trueVec: np.ndarray = np.array(correctList, dtype=np.int8)
    predVec: np.ndarray = np.array(predList, dtype=np.int8)

    # Some of the formulas are at the following link
    # mcc: float = ((tp * tn) - (fp * fn))/ ()
    # TP + TN / P + N
    mcc: float = metrics.matthews_corrcoef(trueVec, predVec)
    trueVecLabels: List[str] = ["Fastest" if x == 1 else "Slowest" for x in trueVec]
    predVecLabels: List[str] = ["Fastest" if x == 1 else "Slowest" for x in predVec]
    accuracy: float = metrics.accuracy_score(trueVec, predVec, normalize=True, sample_weight=None)
    print(confusionMtx)

    # confMtx: np.ndarray = metrics.confusion_matrix(trueVec, predVec, labels=[1, 0])
    confMtx: np.ndarray = metrics.confusion_matrix(trueVecLabels, predVecLabels, labels=["Fastest", "Slowest"])
    print(type(confMtx))
    print(confMtx)
    print(confMtx.shape)
    print(confMtx[0, 1])

    tn, fp, fn, tp = confMtx.ravel()
    print(tn, fp, fn, tp)

    print("\nAccuracy results:")
    print(f"P:\t{sum(trueVec)}")
    print(f"N:\t{len(trueVec) - sum(trueVec)}")
    print(f"ACC:\t{accuracy*100.:.2f}")
    print(f"MCC:\t{mcc*100.:.2f}")
    print(f"PredList:\tfastest={sum(predList)}\tslowest={len(predList)-sum(predList)}\ttotal={len(predList)}")
    print(f"correctList:\tfastest={sum(correctList)}\tslowest={len(correctList)-sum(correctList)}\ttotal={len(correctList)}")

    # Obtain a summary of the predictions
    report = metrics.classification_report(trueVec, predVec, labels=[0, 1], target_names=["slowest", "fastest"], sample_weight=None, digits=4, output_dict=False, zero_division='warn')

    print(report)

    # Load predictions
    return (errTbl, confusionMtx)



def compute_fasta_stats(inFasta: str) -> Tuple[int, int, float, float]:
    """
    Compute information that will be used as feature.
    These information also include the average molecular weight.
    """
    logging.debug(f"\ncompute_fasta_stats :: START")
    logging.debug(f"FASTA input file: {inFasta}")

    # Amino acid masses are taken from the following page
    # https://en.wikipedia.org/wiki/Amino_acid#Expanded_genetic_code
    # Dictionary with the molecular masses
    masses: Dict[str, float] = {"A": 89.094, "C": 121.154, "D": 133.104, "E": 147.131, "F": 165.192,
              "G": 75.067, "H": 155.156, "I": 131.175, "K": 146.189, "L": 131.175,
              "M": 149.208, "N": 132.119, "P": 115.132, "Q": 146.146, "R": 174.203,
              "S": 105.093, "T": 119.119, "V": 117.148, "W": 204.228, "Y": 181.191, "U":168.064}

    # mass of B  is avg(D, N)
    masses["B"] = (masses["D"] + masses["N"]) / 2.
    # mass of Z  is avg(E, Q)
    masses["Z"] = (masses["E"] + masses["Q"]) / 2.
    # mass of J  is avg(I, L)
    masses["J"] = (masses["I"] + masses["L"]) / 2.

    # counters and dictionaries
    seqCnt: int = 0
    proteomeSize: int = 0
    protLengths: List[int] = []
    molMasses: List[float] = []
    tmpLen: int = 0
    molMass: float = 0.
    # generate the text file for the mapping

    # contain the missing amino acids
    missingAA: Set[str] = set()

    # start reading the file
    from Bio import SeqIO
    for seq_record in SeqIO.parse(open(inFasta), "fasta"):
        tmpLen = len(seq_record)
        sequence = str(seq_record.seq)
        sequence = sequence.upper()
        seqCnt += 1
        proteomeSize += tmpLen
        protLengths.append(tmpLen)
        # compute the molecular weight
        for aa in sequence:
            if aa in masses:
                molMass += masses[aa]
            else:
                missingAA.add(aa)
        # add the protein weight
        molMasses.append(molMass)
        molMass = 0.

    # Compute other features
    avgProtLen: float = np.array(protLengths).mean()
    avgMolMass: float = np.array(molMasses).mean()
    
    logging.debug(f"seqCnt:\t{seqCnt}")
    logging.debug(f"proteomeSize:\t{proteomeSize}")
    logging.debug(f"avgProtLen:\t{avgProtLen:.2f}")
    logging.debug(f"avgMolMass:\t{avgMolMass:.2f}")
    logging.debug(f"Missing molecules: {missingAA}")

    # return the genome size
    return (seqCnt, proteomeSize, avgProtLen, avgMolMass)


def generate_categorical_training_data(exTimeDict: dict[str, int], spInfoDict: dict[str, tuple[int, int, float, float]], outTbl: str):
    """Generate the final training data, and use categories (1|0) for fetures containing genome differences."""
    logging.info(f"generate_categorical_training_data :: START")
    logging.info(f"Ex time samples:\t{len(exTimeDict)}")
    logging.info(f"Species info entries:\t{len(spInfoDict)}")
    logging.info(f"Output training data table: {outTbl}")

    # Compute the required features
    invPair: str = ""
    sp1: str = ""
    sp2: str = ""
    exTimePair: float = 0
    exTimeInvPair: float = 0
    exTimeDiffFolds: float = 0.
    protCntSp1: int = 0
    protCntSp2: int = 0
    proteomeSizeSp1: int = 0
    proteomeSizeSp2: int = 0
    avgProtLenSp1: float = 0.
    avgProtLenSp2: float = 0.
    avgMolecularMassSp1: float = 0.
    avgMolecularMassSp2: float = 0.
    seq_cnt_diff_folds_b_gt_a: float = 0.
    prot_size_diff_folds_b_gt_a: float = 0.
    avg_seq_len_diff_folds_b_gt_a: float = 0.
    mol_mass_diff_folds_b_gt_a: float = 0.
    # The class can be one of the following 9:
    # ++, --, -+. +-, =+. +=, -=. =-, ==
    combinationClass: str = ""
    combinationClassList: List[str] = ["", "", ""]
    combinationClassInv: str = ""
    combinationClassInvList: List[str] = ["", "", ""]

    # This would be the label for the training data
    labelPair: int = 0
    labelInvPair: int = 0

    # in the previous model the following features were kept
    # Compute the following features
    # size_a
    # size_b
    # seq_diff_folds_b_gt_a
    # size_diff_folds_b_gt_a
    # avg_len_a
    # avg_len_b

    # Generate all the possible combinations
    pairsList: List[str] = [f"{str(tpl[0])}-{str(tpl[1])}" for tpl in combinations(spInfoDict.keys(), 2)]

    # Make sure the number of combinations is half the number pairs with execution time
    if len(pairsList)*2 != len(exTimeDict):
        logging.error("The number of combinations must be the same as the pairs for which execution time is available.\n")
        print(f"\nlen(pairsList):\t{len(pairsList)}")
        print(f"\nlen(exTimeDict):\t{len(exTimeDict)}")
        exit(-7)

    # Create the output table and write the hdr
    ofd: TextIO = open(outTbl, "wt")
    outTblHdr: str = "pair\textime\textime_diff_folds\tseq_cnt_a\tseq_cnt_b\tseq_cnt_diff_folds_b_gt_a\tproteome_size_a\tproteome_size_b\tprot_size_diff_folds_b_gt_a\tavg_seq_len_a\tavg_seq_len_b\tavg_seq_len_diff_folds_b_gt_a\tmol_mass_a\tmol_mass_b\tmol_mass_diff_folds_b_gt_a\tfolds_combination_class\tfastest_pair\n"

    ofd.write(outTblHdr)

    # Start adding the lines to the output table
    for pair in pairsList:
        sp1, sp2 = pair.split("-", 1)
        invPair = f"{sp2}-{sp1}"
        # Extract features from Sp1
        protCntSp1, proteomeSizeSp1, avgProtLenSp1, avgMolecularMassSp1 = spInfoDict[sp1]
        # Extract features from Sp2
        protCntSp2, proteomeSizeSp2, avgProtLenSp2, avgMolecularMassSp2 = spInfoDict[sp2]

        # print(f"\n{pair}")
        # Compute extra features
        seq_cnt_diff_folds_b_gt_a = check_magnitude_difference(protCntSp1, protCntSp2, 1., debug=False)[1]
        prot_size_diff_folds_b_gt_a = check_magnitude_difference(proteomeSizeSp1, proteomeSizeSp2, 1., debug=False)[1]
        avg_seq_len_diff_folds_b_gt_a = check_magnitude_difference(avgProtLenSp1, avgProtLenSp2, 1., debug=False)[1]
        mol_mass_diff_folds_b_gt_a = check_magnitude_difference(avgMolecularMassSp1, avgMolecularMassSp2, 1., debug=False)[1]

        # check that the values are all positive
        # unless this was not specifically allowed (pvo == False)

        # Seq cnt diff
        if seq_cnt_diff_folds_b_gt_a < 0:
            seq_cnt_diff_folds_b_gt_a = 0
        elif seq_cnt_diff_folds_b_gt_a > 0:
            seq_cnt_diff_folds_b_gt_a = 1
        else:
            sys.exit(f"seq_cnt_diff_folds_b_gt_a [{seq_cnt_diff_folds_b_gt_a}] should not be 0!")
        # Proteome size difference
        if prot_size_diff_folds_b_gt_a < 0:
            prot_size_diff_folds_b_gt_a = 0
        elif prot_size_diff_folds_b_gt_a > 0:
            prot_size_diff_folds_b_gt_a = 1
        else:
            sys.exit(f"prot_size_diff_folds_b_gt_a [{prot_size_diff_folds_b_gt_a}] should not be 0!")
        # Avg length difference
        if avg_seq_len_diff_folds_b_gt_a < 0:
            avg_seq_len_diff_folds_b_gt_a = 0
        elif avg_seq_len_diff_folds_b_gt_a > 0:
            avg_seq_len_diff_folds_b_gt_a = 1
        else:
            sys.exit(f"avg_seq_len_diff_folds_b_gt_a [{avg_seq_len_diff_folds_b_gt_a}] should not be 0!")

        # Extract the execution times
        exTimePair = exTimeDict[pair]
        exTimeInvPair  = exTimeDict[invPair]

        # Set the labels for the pairs
        if exTimePair == exTimeInvPair:
            labelPair = 1
            labelInvPair = 1
            exTimeDiffFolds = 1
        else:
            if exTimePair < exTimeInvPair:
                labelPair = 1
                labelInvPair = 0
                exTimeDiffFolds = check_magnitude_difference(exTimePair, exTimeInvPair, 1., debug=False)[1]
                # Compute the folds for execution times
            else:
                labelPair = 0
                labelInvPair = 1
                exTimeDiffFolds = check_magnitude_difference(exTimeInvPair, exTimePair, 1., debug=False)[1]

        # Set the class for the main pair
        # The first char is the seq count difference
        if seq_cnt_diff_folds_b_gt_a == 1:
            combinationClassList[0] = "+"
            combinationClassInvList[0] = "-"
        else:
            combinationClassList[0] = "-"
            combinationClassInvList[0] = "+"

        # The second char representing proteome size difference
        if prot_size_diff_folds_b_gt_a == 1:
            combinationClassList[1] = "+"
            combinationClassInvList[1] = "-"
        else:
            combinationClassList[1] = "-"
            combinationClassInvList[1] = "+"

        # The third char representing the average seq length difference
        if avg_seq_len_diff_folds_b_gt_a == 1:
            combinationClassList[2] = "+"
            combinationClassInvList[2] = "-"
        else:
            combinationClassList[2] = "-"
            combinationClassInvList[2] = "+"

        # Set the combination class strings
        combinationClass = "".join(combinationClassList)
        combinationClassInv = "".join(combinationClassInvList)

        # Write the line for the main pair
        ofd.write(f"{pair}\t{exTimePair:.2f}\t{exTimeDiffFolds:.2f}\t{protCntSp1}\t{protCntSp2}\t{seq_cnt_diff_folds_b_gt_a:.0f}\t{proteomeSizeSp1}\t{proteomeSizeSp2}\t{prot_size_diff_folds_b_gt_a:.0f}\t{avgProtLenSp1:.2f}\t{avgProtLenSp2:.2f}\t{avg_seq_len_diff_folds_b_gt_a:.0f}\t{avgMolecularMassSp1:.2f}\t{avgMolecularMassSp2:.2f}\t{mol_mass_diff_folds_b_gt_a:.2f}\t{combinationClass}\t{labelPair}\n")
        # Write the line for the inverted pair
        # ofd.write(f"{invPair}\t{exTimeInvPair:.2f}\t{-(exTimeDiffFolds):.2f}\t{protCntSp2}\t{protCntSp1}\t{-(seq_cnt_diff_folds_b_gt_a):.2f}\t{proteomeSizeSp2}\t{proteomeSizeSp1}\t{-(prot_size_diff_folds_b_gt_a):.2f}\t{avgProtLenSp2:.2f}\t{avgProtLenSp1:.2f}\t{-(avg_seq_len_diff_folds_b_gt_a):.2f}\t{avgMolecularMassSp2:.2f}\t{avgMolecularMassSp1:.2f}\t{-(mol_mass_diff_folds_b_gt_a):.2f}\t{combinationClassInv}\t{labelInvPair}\n")

        # Write the line for the inverted pair
        # The relevant values must be adjusted

        # Seq cnt diff
        if seq_cnt_diff_folds_b_gt_a == 0:
            seq_cnt_diff_folds_b_gt_a = 1
        else:
            seq_cnt_diff_folds_b_gt_a = 0
        # Proteome size difference
        if prot_size_diff_folds_b_gt_a == 0:
            prot_size_diff_folds_b_gt_a = 1
        else:
            prot_size_diff_folds_b_gt_a = 0
        # Avg length difference
        if avg_seq_len_diff_folds_b_gt_a == 0:
            avg_seq_len_diff_folds_b_gt_a = 1
        else:
            avg_seq_len_diff_folds_b_gt_a = 0

        ofd.write(f"{invPair}\t{exTimeInvPair:.2f}\t{-(exTimeDiffFolds):.2f}\t{protCntSp2}\t{protCntSp1}\t{seq_cnt_diff_folds_b_gt_a:.0f}\t{proteomeSizeSp2}\t{proteomeSizeSp1}\t{prot_size_diff_folds_b_gt_a:.0f}\t{avgProtLenSp2:.2f}\t{avgProtLenSp1:.2f}\t{avg_seq_len_diff_folds_b_gt_a:.0f}\t{avgMolecularMassSp2:.2f}\t{avgMolecularMassSp1:.2f}\t{mol_mass_diff_folds_b_gt_a:.2f}\t{combinationClassInv}\t{labelInvPair}\n")

    # Close the output file
    ofd.close()



def generate_training_data(exTimeDict: dict[str, int], spInfoDict: dict[str, tuple[int, int, float, float]], pvo: bool, mantissa: int, outTbl: str):
    """Generate the final training data."""
    logging.info(f"generate_training_data :: START")
    logging.info(f"Ex time samples:\t{len(exTimeDict)}")
    logging.info(f"Species info entries:\t{len(spInfoDict)}")
    logging.info(f"Mantissa:\t{mantissa}")
    logging.info(f"Positive values only (pvo):\t{pvo}")
    logging.info(f"Output training data table: {outTbl}")

    # Compute the required features
    invPair: str = ""
    sp1: str = ""
    sp2: str = ""
    exTimePair: float = 0
    exTimeInvPair: float = 0
    exTimeDiffFolds: float = 0.
    protCntSp1: int = 0
    protCntSp2: int = 0
    proteomeSizeSp1: int = 0
    proteomeSizeSp2: int = 0
    avgProtLenSp1: float = 0.
    avgProtLenSp2: float = 0.
    avgMolecularMassSp1: float = 0.
    avgMolecularMassSp2: float = 0.
    seq_cnt_diff_folds_b_gt_a: float = 0.
    prot_size_diff_folds_b_gt_a: float = 0.
    avg_seq_len_diff_folds_b_gt_a: float = 0.
    mol_mass_diff_folds_b_gt_a: float = 0.
    # The class can be one of the following 9:
    # ++, --, -+. +-, =+. +=, -=. =-, ==
    combinationClass: str = ""
    combinationClassList: List[str] = ["", "", ""]
    combinationClassInv: str = ""
    combinationClassInvList: List[str] = ["", "", ""]

    # This would be the label for the training data
    labelPair: int = 0
    labelInvPair: int = 0

    # in the previous model the following features were kept
    # Compute the following features
    # size_a
    # size_b
    # seq_diff_folds_b_gt_a
    # size_diff_folds_b_gt_a
    # avg_len_a
    # avg_len_b

    # Generate all the possible combinations
    pairsList: List[str] = [f"{str(tpl[0])}-{str(tpl[1])}" for tpl in combinations(spInfoDict.keys(), 2)]

    # Make sure the number of combinations is half the number pairs with execution time
    if len(pairsList)*2 != len(exTimeDict):
        logging.error("The number of combinations must be the same as the pairs for which execution time is available.\n")
        print(f"\nlen(pairsList):\t{len(pairsList)}")
        print(f"\nlen(exTimeDict):\t{len(exTimeDict)}")
        exit(-7)

    # Create the output table and write the hdr
    ofd: TextIO = open(outTbl, "wt")
    outTblHdr: str = "pair\textime\textime_diff_folds\tseq_cnt_a\tseq_cnt_b\tseq_cnt_diff_folds_b_gt_a\tproteome_size_a\tproteome_size_b\tprot_size_diff_folds_b_gt_a\tavg_seq_len_a\tavg_seq_len_b\tavg_seq_len_diff_folds_b_gt_a\tmol_mass_a\tmol_mass_b\tmol_mass_diff_folds_b_gt_a\tfolds_combination_class\tfastest_pair\n"

    ofd.write(outTblHdr)

    # Start adding the lines to the output table
    for pair in pairsList:
        sp1, sp2 = pair.split("-", 1)
        invPair = f"{sp2}-{sp1}"
        # Extract features from Sp1
        protCntSp1, proteomeSizeSp1, avgProtLenSp1, avgMolecularMassSp1 = spInfoDict[sp1]
        # Extract features from Sp2
        protCntSp2, proteomeSizeSp2, avgProtLenSp2, avgMolecularMassSp2 = spInfoDict[sp2]

        # print(f"\n{pair}")
        # Compute extra features
        seq_cnt_diff_folds_b_gt_a = check_magnitude_difference(protCntSp1, protCntSp2, 1., debug=False)[1]
        prot_size_diff_folds_b_gt_a = check_magnitude_difference(proteomeSizeSp1, proteomeSizeSp2, 1., debug=False)[1]
        avg_seq_len_diff_folds_b_gt_a = check_magnitude_difference(avgProtLenSp1, avgProtLenSp2, 1., debug=False)[1]
        mol_mass_diff_folds_b_gt_a = check_magnitude_difference(avgMolecularMassSp1, avgMolecularMassSp2, 1., debug=False)[1]

        # check that the values are all positive
        # unless this was not specifically allowed (pvo == False)
        if pvo:
            # Seq cnt diff
            seq_cnt_diff_folds_b_gt_a = mantissa + seq_cnt_diff_folds_b_gt_a
            if seq_cnt_diff_folds_b_gt_a < 0:
                logging.error(f"PVO is True but seq_cnt_diff_folds_b_gt_a is negative ({seq_cnt_diff_folds_b_gt_a})\nTry to increase the mantissa (now is {mantissa}).")
                print(protCntSp1, protCntSp2, seq_cnt_diff_folds_b_gt_a, mantissa, seq_cnt_diff_folds_b_gt_a - mantissa)
                sys.exit(-10)
            # Proteome size difference
            prot_size_diff_folds_b_gt_a = mantissa + prot_size_diff_folds_b_gt_a
            if prot_size_diff_folds_b_gt_a < 0:
                logging.error(f"PVO is True but prot_size_diff_folds_b_gt_a is negative ({prot_size_diff_folds_b_gt_a})\nTry to increase the mantissa (now is {mantissa}).")
                print(proteomeSizeSp1, proteomeSizeSp2, prot_size_diff_folds_b_gt_a, mantissa, prot_size_diff_folds_b_gt_a - mantissa)
                sys.exit(-10)
            # Avg length difference
            avg_seq_len_diff_folds_b_gt_a = mantissa + avg_seq_len_diff_folds_b_gt_a
            if avg_seq_len_diff_folds_b_gt_a < 0:
                logging.error(f"PVO is True but avg_seq_len_diff_folds_b_gt_a is negative ({avg_seq_len_diff_folds_b_gt_a})\nTry to increase the mantissa (now is {mantissa}).")
                print(avgProtLenSp1, avgProtLenSp2, avg_seq_len_diff_folds_b_gt_a, mantissa, avg_seq_len_diff_folds_b_gt_a - mantissa)
                sys.exit(-10)
            # Avg length difference
            mol_mass_diff_folds_b_gt_a = mantissa + mol_mass_diff_folds_b_gt_a
            if mol_mass_diff_folds_b_gt_a < 0:
                logging.error(f"PVO is True but mol_mass_diff_folds_b_gt_a is negative ({mol_mass_diff_folds_b_gt_a})\nTry to increase the mantissa (now is {mantissa}).")
                print(avgMolecularMassSp1, avgMolecularMassSp2, mol_mass_diff_folds_b_gt_a, mantissa, mol_mass_diff_folds_b_gt_a - mantissa)
                sys.exit(-10)

        # Extract the execution times
        exTimePair = exTimeDict[pair]
        exTimeInvPair  = exTimeDict[invPair]

        # Set the labels for the for the pairs
        if exTimePair == exTimeInvPair:
            labelPair = 1
            labelInvPair = 1
            exTimeDiffFolds = 1
        else:
            if exTimePair < exTimeInvPair:
                labelPair = 1
                labelInvPair = 0
                exTimeDiffFolds = check_magnitude_difference(exTimePair, exTimeInvPair, 1., debug=False)[1]
                # Compute the folds for execution times
            else:
                labelPair = 0
                labelInvPair = 1
                exTimeDiffFolds = check_magnitude_difference(exTimeInvPair, exTimePair, 1., debug=False)[1]

        # Set the class for the main pair
        # The first char is the seq count difference
        if seq_cnt_diff_folds_b_gt_a == 1 + mantissa:
            combinationClassList[0] = "="
            combinationClassInvList[0] = "="
        elif seq_cnt_diff_folds_b_gt_a > 1 + mantissa:
            combinationClassList[0] = "+"
            combinationClassInvList[0] = "-"
        else:
            combinationClassList[0] = "-"
            combinationClassInvList[0] = "+"

        # The second char representing proteome size difference
        if prot_size_diff_folds_b_gt_a == 1 + mantissa:
            combinationClassList[1] = "="
            combinationClassInvList[1] = "="
        elif prot_size_diff_folds_b_gt_a > 1 + mantissa:
            combinationClassList[1] = "+"
            combinationClassInvList[1] = "-"
        else:
            combinationClassList[1] = "-"
            combinationClassInvList[1] = "+"

        # The third char representing the average seq length difference
        if avg_seq_len_diff_folds_b_gt_a == 1 + mantissa:
            combinationClassList[2] = "="
            combinationClassInvList[2] = "="
        elif avg_seq_len_diff_folds_b_gt_a > 1 + mantissa:
            combinationClassList[2] = "+"
            combinationClassInvList[2] = "-"
        else:
            combinationClassList[2] = "-"
            combinationClassInvList[2] = "+"

        # Set the combination class strings
        combinationClass = "".join(combinationClassList)
        combinationClassInv = "".join(combinationClassInvList)

        # Write the line for the main pair
        ofd.write(f"{pair}\t{exTimePair:.2f}\t{exTimeDiffFolds:.2f}\t{protCntSp1}\t{protCntSp2}\t{seq_cnt_diff_folds_b_gt_a:.2f}\t{proteomeSizeSp1}\t{proteomeSizeSp2}\t{prot_size_diff_folds_b_gt_a:.2f}\t{avgProtLenSp1:.2f}\t{avgProtLenSp2:.2f}\t{avg_seq_len_diff_folds_b_gt_a:.2f}\t{avgMolecularMassSp1:.2f}\t{avgMolecularMassSp2:.2f}\t{mol_mass_diff_folds_b_gt_a:.2f}\t{combinationClass}\t{labelPair}\n")
        # Write the line for the inverted pair
        # ofd.write(f"{invPair}\t{exTimeInvPair:.2f}\t{-(exTimeDiffFolds):.2f}\t{protCntSp2}\t{protCntSp1}\t{-(seq_cnt_diff_folds_b_gt_a):.2f}\t{proteomeSizeSp2}\t{proteomeSizeSp1}\t{-(prot_size_diff_folds_b_gt_a):.2f}\t{avgProtLenSp2:.2f}\t{avgProtLenSp1:.2f}\t{-(avg_seq_len_diff_folds_b_gt_a):.2f}\t{avgMolecularMassSp2:.2f}\t{avgMolecularMassSp1:.2f}\t{-(mol_mass_diff_folds_b_gt_a):.2f}\t{combinationClassInv}\t{labelInvPair}\n")

        # Write the line for the inverted pair
        # The mantissa must be used to adjust the values
        ofd.write(f"{invPair}\t{exTimeInvPair:.2f}\t{-(exTimeDiffFolds):.2f}\t{protCntSp2}\t{protCntSp1}\t{(mantissa - seq_cnt_diff_folds_b_gt_a) + mantissa:.2f}\t{proteomeSizeSp2}\t{proteomeSizeSp1}\t{(mantissa - prot_size_diff_folds_b_gt_a) + mantissa:.2f}\t{avgProtLenSp2:.2f}\t{avgProtLenSp1:.2f}\t{(mantissa - avg_seq_len_diff_folds_b_gt_a) + mantissa:.2f}\t{avgMolecularMassSp2:.2f}\t{avgMolecularMassSp1:.2f}\t{(mantissa - mol_mass_diff_folds_b_gt_a) + mantissa:.2f}\t{combinationClassInv}\t{labelInvPair}\n")



    # Close the output file
    ofd.close()



def load_ex_times(exTimetbl: str, snapshotPath: str) -> Tuple[Dict[str, float], Dict[str, Tuple[int, int, float, float]]]:
    """
    Load executiohn times from sonicparanoid.
    """
    logging.info(f"load_ex_times :: START")
    logging.info(f"Ex times raw table: {exTimetbl}")
    logging.info(f"Table with species information from SonicParanoid run: {snapshotPath}")

    # Set temp variables
    exTimeDict: Dict[str, float] = {}
    flds: List[str] = []
    pairRaw: str = ""
    pair: str = ""
    extimeRaw: str = ""
    extime: float = 0.
    # Dictionaries with species information
    # It will contain:
    # Protein count, proteome size, avg protein length, amino acids per protein sequence
    spInfoDict: Dict[str, Tuple[int, int, float, float]] = {}
    mappingDict: Dict[str, str] = {}
    spName: str = ""
    spId: str = ""
    # tmpStr: str = ""
    protCnt: int = 0
    protSize: int = 0
    sp1: str =""
    sp2: str = ""

    # Load species information
    # Lines in the snapshot have the following format
    # sp_id species file_hash protein_cnt proteome_size
    # 6 clupus 84a09b94943956211e53faf2df1c0b76e2459910541ff237b8faaffdd15720c9 20649 11583642
    with open(snapshotPath, "rt") as ifd:
        for ln in ifd:
            ln = ln.rstrip("\n")
            # print(ln)
            flds = ln.split("\t", 4)
            spId = flds[0]
            spName = flds[1]
            protCnt = int(flds[3])
            protSize = int(flds[4])
            # print(spId, spName, protCnt, protSize)
            # Add elements to mapping file
            mappingDict[spId] = spName
            # The last 2 values will updated in later step
            spInfoDict[spName] = (protCnt, protSize, 0., 0.)

    # for sp, tpl in spInfoDict.items():
    #     print(f"{sp}:\t{tpl}")

    # Line in the execution time have the following format
    # pair execution-time conversion-time parsing-time query-pct-used target-pct-used essensial-set-creation-time
    # 13-6 847.410 1.000 8.300 100.00 100.00 0.000
    # open the file and extrac the required information
    with open(exTimetbl, "rt") as ifd:
        for ln in ifd:
            ln = ln.rstrip("\n")
            # print(ln)
            pairRaw, extimeRaw = ln.split("\t", 2)[:2]
            extime = float(extimeRaw)
            sp1, sp2 = pairRaw.split("-", 1)
            # Skip intraspecies alignments
            if sp1 == sp2:
                continue

            pair = f"{mappingDict[sp1]}-{mappingDict[sp2]}"
            # print(f"{pair}:\t{extime}")
            if not pair in exTimeDict:
                exTimeDict[pair] = extime
            else:
                sys.stderr.write(f"\nERROR: the pair {pair} is repeated!")
                sys.exit(-7)
            # break

    return (exTimeDict, spInfoDict)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    rawTbl = os.path.realpath(args.raw_ex_time)
    snapshotPath = os.path.realpath(args.snapshot)
    outDir = os.path.realpath(args.output_dir)
    fastaDir: str = os.path.realpath(args.fasta_dir)
    categorical: bool = args.categorical
    predTblPath: str = ""
    if len(args.prediction_tbl) > 0:
        predTblPath = os.path.realpath(args.prediction_tbl)
    # create output directories
    makedir(outDir)
    accuracyOnly: bool = args.accuracy_only
    # omly allow positive values
    pvo: bool = not args.allow_negative_values
    mantissa: int = args.mantissa
    if pvo:
        if mantissa <= 0:
            logging.error(f"Only positive values are allowed but the mantissa is not positive ({mantissa}).\nIncrease the mantissa or set the --positive-values-only parameter to False.")
            sys.exit(-2)
    else:
        logging.info("\nNegative values are allowed and the mantissa will be set to 0.")
        mantissa = 0

    # set the output file path
    outName: str = "adaboost_training_data_raw"
    if categorical:
        outName = f"{outName}.cat."
    else:
        # write infor about the mantissa
        outName = f"{outName}.mantissa{mantissa}."
    # output table path
    # outTbl: str = os.path.join(outDir, f"adaboost_training_data_raw.{os.path.basename(rawTbl)}")
    outTbl: str = os.path.join(outDir, f"{outName}{os.path.basename(rawTbl)}")

    # Show some info
    logging.info(f"Raw execution time table: {rawTbl}")
    logging.info(f"Snapshot file from SonicParanoid: {snapshotPath}")
    logging.info(f"Directory with input FASTA files: {fastaDir}")
    logging.info(f"Prediction table: {predTblPath}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Output training samples: {outTbl}")
    logging.info(f"Positive values only:\t{pvo}")
    logging.info(f"Use categories (0|1) for diff features:\t{categorical}")
    logging.info(f"Mantissa (used in computation of folds):\t{mantissa}")

    # Load execution times
    exTimeDict, spInfoDict = load_ex_times(rawTbl, snapshotPath)

    # Write some debug infomation
    print(f"\nLoaded ex times:\t{len(exTimeDict)}")
    print(f"Species information from snapshot file:\t{len(spInfoDict)}")

    # print(accuracyOnly)
    # print(not accuracyOnly)

    if not accuracyOnly:
        # Compute some simple stats from the input FASTA files
        tmpFastaPath: str = ""
        tmpTpl: Tuple[int, int, float, float] = (0, 0, 0., 0.)
        for sp, spInfo in spInfoDict.items():
            tmpFastaPath = os.path.join(fastaDir, sp)
            if not os.path.isfile(tmpFastaPath):
                logging.error(f"The FASTA file is missing for species \n{sp}")
                exit(-2)
            else:
                # compute simple stats
                tmpTpl = compute_fasta_stats(tmpFastaPath)
                if (tmpTpl[0] != spInfo[0]) or (tmpTpl[1] != spInfo[1]):
                    logging.error(f"The protein counts or proteome sizes has changes when compared to the snapshot! Check again for species {sp}.")
                else:
                    # update the info dictionary
                    spInfoDict[sp] = tmpTpl

        # Create the table with the training data
        if categorical:
            generate_categorical_training_data(exTimeDict, spInfoDict, outTbl)
        else:
            generate_training_data(exTimeDict, spInfoDict, pvo, mantissa, outTbl)

    # Only compute the accuracy assuming the training data is already available
    else:
        print(not os.path.isfile(outTbl))
        if not os.path.isfile(outTbl):
            logging.error("The table with the training data is missing! Remove the --accuracy-only to create the training data table.\n")
            exit(-2)
    # Compute confusion matrix and other stats
    if os.path.isfile(predTblPath):
        compute_accuracy(outTbl, predTblPath, snapshotPath, exTimeDict)
    else:
        logging.info("No predictions were provided. The compution of accuracy will be skipped.")



if __name__ == "__main__":
    main()

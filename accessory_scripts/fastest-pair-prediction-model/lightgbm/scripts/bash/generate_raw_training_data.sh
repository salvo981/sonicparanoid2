date

# NOTE: the following is the directory with FASTA files from the SonicParanoid2 paper
# The link the files in ZENODO can be found in the SonicParanoid2 paper in Genome Biology
# ~/Desktop/adaboost_training/reference250processed/

# generate training samples and labels from execution times
echo $'\n@@@@ generate-raw-tr-data using categorical labels @@@@'
python3 ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/lightgbm/scripts/python/generate_raw_tr_data.py -r ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/aln-ex-times/aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv -s ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/snapshot.250_proteomes.azur.tsv -f ~/Desktop/adaboost_training/reference250processed/ -o ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/lightgbm/raw-training-data/ --categorical

# All done
date
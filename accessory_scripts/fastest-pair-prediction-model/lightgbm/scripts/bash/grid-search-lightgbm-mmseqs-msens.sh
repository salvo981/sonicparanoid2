date

# Use 9 features (categorical data)
echo $'\n@@@@ Grid search for LightGBM on 9 features using tr-data from mmseqs-msens @@@@'
python3 ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/lightgbm/scripts/python/lightgbm_grid_search_from_raw_tr_data.py -r ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/lightgbm/raw-training-data/adaboost_training_data_raw.cat.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/lightgbm/ --no-mol-weights --threads 8 --out-prefix lightbgm.mmseqs_msens.no-weights.cat &> ~/work_repos/sonicparanoid2/accessory_scripts/fastest-pair-prediction-model/lightgbm/logs/log.lightbgm.mmseqs_msens.no-weights.cat.txt
wait

# All runs completed
date

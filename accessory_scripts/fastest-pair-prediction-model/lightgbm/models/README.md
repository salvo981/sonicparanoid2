### lightgbm model trained on 17/06/24

The model was trained using the following python modules:
- lightbgm 4.4.0
- scikit-learn 1.5.0

### How to train the model


#### Generate raw training data
> NOTE: this step is not need if the raw training table is already available under `/fastest-pair-prediction-model/lightgbm/raw-training-data/`  
The training data is generated using the script `./fastest-pair-prediction-model/lightgbm/scripts/bash/generate_raw_training_data.sh`

#### Train the lightbgm model

The model is trained using grid-search using the script `./fastest-pair-prediction-model/lightgbm/scripts/bash/grid-search-lightgbm-mmseqs-msens.sh`


#### Load and test the model using ipython

Following is a code snipplet to load and test the model.  

```
import os, sys
import lightbgm as lbg
import numpy as np
from pickle import load

# load the model
model = load(open("./lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl", "rb"))

# Samples for the model have the following format, and given in a 2d numpy vector
# seq_cnt_a seq_cnt_b seq_cnt_diff_folds_b_gt_a proteome_size_a proteome_size_b prot_size_diff_folds_b_gt_a # avg_seq_len_a avg_seq_len_b avg_seq_len_diff_folds_b_gt_a
# 11501 11204 0 6025983 4609745 0 523.95 411.44 0
# 4791 11501 1 1451551 6025983 1 302.97 523.95 1

# Create a sample as an array
a1 = np.array([11501, 11204, 0, 6025983, 4609745, 0, 523.95, 411.44, 0])

# perform the prediction on a1
# NOTE: it must be a 2D array
model.predict([a1])

# Out[9]: array([0])

# The sample was predicted as slowest

```

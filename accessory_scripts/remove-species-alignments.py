# -*- coding: utf-8 -*-
"""Delete intra-proteome alignment files."""
import os
import sys
import argparse
import platform
import numpy as np
from typing import Dict, List, Deque, Any, Tuple, Set, TextIO



########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    parser = argparse.ArgumentParser(description=f"Write some stats on the selected hits", usage='%(prog)s -i <INPUT_ALIGNMENT> [options]', prog="count_above_thr_hits.py")

    # Mandatory arguments
    parser.add_argument("-a", "--aln-dir", type=str, required=True, help="File with alignment results file.", default=None)
    parser.add_argument("-s", "--snapshot", type=str, required=True, help="File with species information generate using SonicParanoid.", default="")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information. (WARNING: extremely verbose)", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



########### MAIN ############
def main():

    #Get the parameters
    args, parser = get_params()
    del parser
    # start setting the needed variables
    debug = args.debug
    # input alignment file
    alnDirPath: str = os.path.realpath(args.aln_dir)
    snapshotPath: str = os.path.realpath(args.snapshot)
    # species set
    spSet: Set[str] = set()
    removedSet: Set[str] = set()
    sp: str = ""
    toRemoveAln: str = ""

    if debug:
        print("\nExecution info:")
        print(f"Alignments dir: {alnDirPath}")
        print(f"Snapshot file: {snapshotPath}")

    # Variable to keep count of values
    ifd: TextIO = open(snapshotPath, "rt")
    # Example of line in snapshot file
    # 1 chlamydia_trachomatis aefdc6e98c92e4c6181720c5576f034a14ebf062446d56bbdb073b319b394d91 917 315378
    for ln in ifd:
        sp = ln[:-1].split("\t", 1)[0]
        # add species to set
        spSet.add(sp)
        # create the path to intraspecies alignment
        toRemoveAln = os.path.join(alnDirPath, f"{sp}/{sp}-{sp}")
        if os.path.isfile(toRemoveAln):
            os.remove(toRemoveAln)
            removedSet.add(os.path.basename(toRemoveAln))

    print("\nAnalysis summary:")
    print(f"Found species:\t{len(spSet)}")
    print(f"Removed intra-proteome alignments:\t{len(removedSet)}")


if __name__ == "__main__":
    main()


# using Base: iscontiguous, Float64
#=
Procees archives with datapoints from qfo20 benchmark and generate datapoints for plotting.
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
# using DataFrames
using Logging
using SHA

# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using CompareOrthologTables



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "orthog-db1"
            nargs = 1
            arg_type = String
            help = "Directory containing the ortholog tables for each pair."
            required = true
        "orthog-db2"
            nargs = 1
            arg_type = String
            help = "Directory containing the segond set of ortholog tables."
            required = true
        # "--output-dir"
        #     nargs = '?'
        #     arg_type = String
        #     default = "."
        #     help = "Directory that will contain the output files."
        #     required = true
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""
Compupe and compare the hashes for all pairs and tables
"""
function compare_hashes(pairs2paths::Dict{String, Tuple{String, String}})::Dict{String, Tuple{String, String}}
    @debug "compare_hashes :: START" length(pairs2paths)

    # Functions from SHA.jl are used

    # tmp variables
    differingFiles::Dict{String, Tuple{String, String}} = Dict{String, Tuple{String, String}}()
    path1::String = path2 = ""
    pair::String = ""
    sha256p1 = Array{UInt8, 1}()
    sha256p2 = Array{UInt8, 1}()

    for (protPair, paths) in pairs2paths
        path1, path2 = paths
        # compute hash for the first file
        open(path1) do f
            sha256p1 = sha2_256(f)
            # @show sha256p1
        end
        # compute hash for the second file
        open(path2) do f
            sha256p2 = sha2_256(f)
            # @show sha256p2
        end

        # Compare the hashes
        if sha256p1 != sha256p2
            differingFiles[protPair] = paths
        end

    end

    @debug "Summary on hash comparison" length(pairs2paths) length(differingFiles)

    return differingFiles

end





"""
Compute simple stats and accuracy measures based on the input datapoints.
"""
function load_table_paths(db1::String, db2::String)::Dict{String, Tuple{String, String}}
    @debug "load_table_paths :: START" db1 db2

    # tmp variables
    tablesDict1::Dict{String, String} = Dict{String, String}()
    tablesDict2::Dict{String, String} = Dict{String, String}()
    tbl2paths::Dict{String, Tuple{String, String}} = Dict{String, Tuple{String, String}}()
    path::String = ""
    pair::String = ""

    # Load tables for DB1
    for (root, dirs, files) in walkdir(db1)
        for file in files
            pair = rsplit(file, ".", limit=2)[2]
            path = joinpath(root, file)

            # add entries to the dictionary
            if !haskey(tablesDict1, pair)
                tablesDict1[pair] = path
            else
                @error "Pair $(pair) was found multiple times!" path
                exit(-5)
            end
        end
    end

    @debug "Loaded tables for DB1" length(tablesDict1)

    # Load tables for DB2
    for (root, dirs, files) in walkdir(db2)
        for file in files
            pair = rsplit(file, ".", limit=2)[2]
            path = joinpath(root, file)
            # add entries to the dictionary
            if !haskey(tablesDict2, pair)
                tablesDict2[pair] = path
            else
                @error "Pair $(pair) was found multiple times!" path
                exit(-5)
            end
        end
    end

    @debug "Loaded tables for DB2" length(tablesDict2)

    
    # load paths in teh output dictionary
    for (p, fpath) in tablesDict1
        tbl2paths[p] = (fpath, tablesDict2[p])
    end
    
    # @show "Table paths" tablesDict1["1-2"] tablesDict2["1-2"] tbl2paths["1-2"]

    return tbl2paths

end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

@show args["orthog-db1"][1] args["orthog-db2"][1]
db1 = realpath(args["orthog-db1"][1])
db2 = realpath(args["orthog-db2"][1])
# outDir = abspath(args["output-dir"])
# makedir(outDir)
# outTbl = abspath(args["output-tbl"])
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Orhtolog tables will be compare with the following parameters:" db1 db2 debug

# Load the tables
pair2paths = load_table_paths(db1, db2)

# Compare the hashes for wach pair of tables
differingFiles = compare_hashes(pair2paths)


for (pair, paths) in differingFiles
    println("$(pair)\t$(filesize(paths[1])/1000)")
    # @show pair
    # break
end

# Compute stats
# compute_stats_and_accuracy(essentialsDatapoints, outDir)
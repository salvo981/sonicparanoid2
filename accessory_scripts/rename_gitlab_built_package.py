"""
 Obtain the path of the build file, an make a copy to be used for testing.
"""

import os
import sys
from shutil import which, copy
import platform
import argparse



########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    parser = argparse.ArgumentParser(description="Identify PyPI build in dir.", usage='%(prog)s -p <BUILD_DIRECTORY>', prog="rename_gitlab_sonicparanoid_build.py")

    # Mandatory arguments
    parser.add_argument("-p", "--pkg-dir", type=str, required=True, help="Directory in which the package is stored.", default=None)
    # parser.add_argument("-t", "--make-test-copy", required=False, help="Generate a copy of the package to be used for testing the installation.\n", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def extract_version(setupFilePath:str) -> str:
    """
    Obtain package version from setup.py
    """
    ver: str = ""
    tmpStr: str = ""
    # The line containing the version have the followibng format:
    # version = "2.0.0a2", # Required
    with open(setupFilePath, "rt") as ifd:
        for ln in ifd:
            ln = ln[:-1]
            if ln.startswith("    version = "):
                # print(ln)
                tmpStr = ln.split("\"", 1)[1]
                # print(tmpStr)
                ver = tmpStr.rsplit("\"", 1)[0]

    return ver




##### MAIN #####
def main() -> None:

    # check OS
    OS = platform.system()

    #Get the parameters
    args, parser = get_params()
    del parser
    # Output directory
    pkgDir: str = os.path.abspath(args.pkg_dir)
    if not os.path.isdir(pkgDir):
        os.makedirs(pkgDir)

    # check python installation
    pythonPath: str = ""
    if which("python3") is None:
        sys.stderr.write("ERROR: the command \"python3\" was not found. Please install python3.")
        sys.exit(-5)
    else:
        pythonPath = which("python3")

    # set log paths
    # get path to the script
    pySrcDir: str = os.path.dirname(os.path.abspath(__file__))
    setupPath: str = os.path.join(os.path.dirname(pySrcDir), "setup.py")
    # check the extans of the setup file
    if not  os.path.isfile(setupPath):
        sys.stderr.write(f"ERROR: the setup file \n{setupPath}\n was not found in the directory\n{pySrcDir}\n")
        sys.exit(-2)

    # extract the version of SonicParanoid
    sonicVer: str = extract_version(setupPath)
    # Identify the path with the sdist package
    pkgPath: str = os.path.join(pkgDir, f"sonicparanoid-{sonicVer}.tar.gz")

    # Print some infomation
    sys.stdout.write("\nrename_gitlab_sonicparanoid.py PARAMS:")
    sys.stdout.write(f"\nOS: {OS}")
    sys.stdout.write(f"\nPython path: {pythonPath}")
    sys.stdout.write(f"\nsetup.py path: {setupPath}")
    sys.stdout.write(f"\nSonicParanoid version: {sonicVer}")
    sys.stdout.write(f"\nPackage directory path: {pkgDir}")
    sys.stdout.write(f"\nPackage directory exists: {os.path.isdir(pkgDir)}")
    sys.stdout.write(f"\nPackage path: {pkgPath}\n")

    # sys.exit("DEBUG")

    if not os.path.isfile(pkgPath):
        sys.stderr.write(f"ERROR: the sdist package was not found in\n{pkgPath}\n")

    # Make a copy of the generated file
    copy(pkgPath, os.path.join(pkgDir, "sonicparanoid.test.tar.gz"))
    print(f"\nINFO: a copy of the package for testing was created in the output directory\n{pkgDir}")


if __name__ == "__main__":
    main()
